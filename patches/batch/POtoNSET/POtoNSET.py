from pathlib import Path
import os
import sys
import shutil
import json
import polib
from datetime import datetime

#Default
file_read_path=Path("MESPAK00-00.POT")

lang_code='es'
language_team="Spanish"
output_json_sufix=".nset"
output_dir_po="PO"
output_dir_json="JSON"

def PO_to_JSON(file_read_path):

        #Create JSON dir
    if not os.path.exists(output_dir_json):
        os.mkdir(output_dir_json)

    po = polib.pofile(file_read_path)
    data={}
    for id,entry in enumerate(po):
#        if entry.msgstr!='':
        entry_str_list=entry.msgstr.split("\n")
        entry_id_list=entry.msgid.split("\n")
#        entry_str_list[0:-1]=[x+"\n" for x in entry_str_list[0:-1]]
#        entry_id_list[0:-1]=[x+"\n" for x in entry_id_list[0:-1]]

#        if entry_str_list==[""]:
#            entry_str_list=entry_id_list

        #print(entry.msgctxt,entry_id_list,entry_str_list)

        data_str={}
        for i in range(len(entry_str_list)):
            #print (entry_str_list[i])
            data_str[str(i+1)]={"text":entry_str_list[i]}

        data_id={}
        for i in range(len(entry_id_list)):
            #print (entry_id_list[i])
            data_id[str(i+1)]={"text":entry_id_list[i]}

        #print(data_id)
        #print(data_str)
        if data_id=={'1': {'text': ''}}:
            data_id=None
            data_str=None

        data[str(id+1)]={
            "extra_space": True,
            "full_width": False,
            "original": data_id,
            "translation": data_str
        }
        #print(data)
        #break
    #print(type(data))
    my_path=str(file_read_path).rsplit('_', 1)[0]
    my_path=Path(my_path)
    json_path=os.path.join(os.getcwd(),output_dir_json,my_path.stem)

    with open(json_path+output_json_sufix, 'w',encoding='utf8') as f:
        json.dump(data, f, indent=4, ensure_ascii=False,sort_keys=False)
    print("Success!")
 
    #fix_crlf_to_lf(my_path.stem+output_json_sufix)

def generate_PO_file(texts,translations,path, lang_code,pot_mode):    
    now = datetime.now()
    time_string = now.astimezone().strftime("%Y-%m-%d %H:%M:%S%z")
    po = polib.POFile()
    po.metadata = {
    'Project-Id-Version': '1.0',
    'Report-Msgid-Bugs-To': 'daru@smolgaming.com',
    'POT-Creation-Date': time_string,
    'PO-Revision-Date': time_string,
    'Last-Translator': 'Daru <daru@smolgaming.com>',
    'Language-Team': language_team+' <daru@smolgaming.com>',
#    'Language': 'ja',
    'Language': lang_code,
    'MIME-Version': '1.0',
    'Content-Type': 'text/plain; charset=UTF-8',
    'Content-Transfer-Encoding': '8bit',
#    'Plural-Forms': 'nplurals=1; plural=0);',
    'Plural-Forms': 'nplurals=2; plural=(n!=1);',
    }

    pot=polib.POFile()
    pot.metadata=po.metadata

    zerof=len(str(len(texts)))
#    for id,text in enumerate(texts):
    for id, (text, trans) in enumerate(zip(texts, translations)):
        #print (len(str(len(texts))))
#        if(trans==text):
#            trans=""
        entry = polib.POEntry(
        msgctxt=str(id).zfill(zerof),
        msgid=text,
#        msgstr=""
        msgstr=trans
        #occurrences=[('welcome.py', '12'), ('anotherfile.py', '34')]
        )
        entry_pot = polib.POEntry(
        msgctxt=str(id).zfill(zerof),
        msgid=text,
        msgstr=""
        #occurrences=[('welcome.py', '12'), ('anotherfile.py', '34')]
        )
        
        po.append(entry)
        pot.append(entry_pot)

    po_path=path
    if pot_mode:
        po_path+='_'+lang_code
    po_path+='.po'

    pot_path=path+'.pot'
 #   pot_path=pot_path.stem
#    pot_path=pot_path

    if pot_mode:
        pot.save(pot_path)
    po.save(po_path)

def fix_crlf_to_lf(path):
    # replacement strings
    WINDOWS_LINE_ENDING = b'\r\n'
    UNIX_LINE_ENDING = b'\n'

    # relative or absolute file path, e.g.:
#    file_path = r"c:\Users\Username\Desktop\file.txt"

    with open(path, 'rb') as open_file:
        content = open_file.read()
    
    # Windows ➡ Unix
    content = content.replace(WINDOWS_LINE_ENDING, UNIX_LINE_ENDING)

    # Unix ➡ Windows
    # content = content.replace(UNIX_LINE_ENDING, WINDOWS_LINE_ENDING)

    with open(path, 'wb') as open_file:
        open_file.write(content)
    
def JSON_to_PO(file_read_path,pot_mode):

    #Create PO dir
    if not os.path.exists(output_dir_po):
        os.mkdir(output_dir_po)
#    else:
#        shutil.rmtree(output_dir)
#        os.mkdir(output_dir)

    f = open(file_read_path,encoding="utf8")
    data = json.load(f)
#    print(data)

    all_original=[]
    all_trans=[]

    for i in range(len(data)):
#        print(ctxt)
        original=""
        if (data[str(i+1)]["original"]!=None):
#            print(str(i+1))
            for id_text in data[str(i+1)]["original"]:
    #            print(data[ctxt]["original"][id_text]["text"])
    #            original.append(data[ctxt]["original"][id_text]["text"])
                original=original+data[str(i+1)]["original"][id_text]["text"]
                if(not original.endswith('\n')):
                    original+="\n"
            original=original[0:-1]

            all_original.append(original)
        else:
            all_original.append(original)

        trans=""
        if (data[str(i+1)]["translation"]!=None):
            for id_text in data[str(i+1)]["translation"]:
    #            print(data[ctxt]["translation"][id_text]["text"])
    #            trans.append(data[ctxt]["translation"][id_text]["text"])      
                trans=trans+data[str(i+1)]["translation"][id_text]["text"]
                if(not trans.endswith('\n')):
                    trans+="\n"
            trans=trans[0:-1]

            all_trans.append(trans)
        else:
            all_trans.append(trans)

#    print(all_original)
#    print(all_trans)

    my_path=Path(file_read_path)
    po_path=os.path.join(os.getcwd(),output_dir_po,my_path.stem)
#    print(po_path)
#    po_path=os.path.join(dir_po,my_path)
    generate_PO_file(all_original,all_trans,po_path, lang_code,pot_mode) #Save file
    print("Success!")


def main(arg1,arg2,arg3):
#    if ((len(sys.argv))==3 or ()):
    if (arg1!='-toJSON' and arg1!='-toPO'):
        print (error_str)            
        exit()
    try:
        global file_read_path
        file_read_path=Path(arg2)
#       global folder_read_path
#       folder_read_path=Path(sys.argv[2])
    except:
        "\tException: Wrong paths!"
    if (arg1=='-toJSON'):
        PO_to_JSON(file_read_path)
    elif (arg1=='-toPO'):
        mode=False
        if arg3=="0":
            mode=False
        elif arg3=="1":
            mode=True
        else:
            print("\tWrong mode!")
            return -1
        JSON_to_PO(file_read_path,mode)

#    global file_read_path
#    file_read_path=Path(sys.argv[1])
#    print(file_read_path)
  

    return 0

if __name__ == "__main__":
    error_str="\n\tWrong arguments!\n\n\t############################\n\n\tTO CONVERT FROM JSON TO PO:\n\n\tpython POtoNSET.py -toJSON --file-path \n\n\tExample:\n\n\tpython POtoJSON.py -toJSON script.po \n\n\t#########################################\n\n\tTO CONVERT FROM JSON TO PO:\n\n\tpython POtoJSON.py -toPO --file-path --pot_mode \n\n\tExample:\n\n\tpython POtoJSON.py -toPO script.po 1"
    if((len(sys.argv)!=4)):
        print (error_str)
        exit()
    sys.exit(main(sys.argv[1], sys.argv[2],sys.argv[3]))
#    main(sys.argv[1], sys.argv[2])

