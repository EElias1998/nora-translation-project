import os
import sys
import POtoNSET
# assign directory
directory = 'files'

print(os.getcwd())
 
# iterate over files in
# that directory
def main():
    for filename in os.listdir(directory):
        f = os.path.join(directory, filename)
        # checking if it is a file
        if os.path.isfile(f):
            print(f)
            POtoNSET.main("-toJSON", f,"0")

if __name__ == "__main__":
    main()